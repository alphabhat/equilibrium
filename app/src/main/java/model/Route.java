package model;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by arubhat on 19/02/17.
 */
public class Route implements Serializable{
    private static final long serialVersionUID = 10L;
    private String path;
    private String pathdraw;
    private String routeName;
    private String openedBy;
    private String difficulty;
    private String coordinates;
    private String date;
    private int routeId;


    public Route(String routeName, int routeId, String openedBy, String difficulty, String coordinates, String date, String path, String pathdraw) {
        this.routeName = routeName;
        this.openedBy = openedBy;
        this.difficulty = difficulty;
        this.coordinates = coordinates;
        this.date = date;
        this.path = path;
        this.routeId = routeId;
        this.pathdraw = pathdraw;
    }

    public Route() {

    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getOpenedBy() {
        return openedBy;
    }

    public void setOpenedBy(String openedBy) {
        this.openedBy = openedBy;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }
    public String getPathdraw() {
        return pathdraw;
    }

    public void setPathdraw(String pathdraw) {
        this.pathdraw = pathdraw;
    }

}
