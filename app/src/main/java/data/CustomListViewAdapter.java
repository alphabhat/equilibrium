package data;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arubhat.routelogger.DisplayRoutesActivity;
import com.example.arubhat.routelogger.R;
import com.example.arubhat.routelogger.RouteDetailsActivity;

import java.util.ArrayList;

import model.Route;
import util.Utils;

/**
 * Created by arubhat on 19/02/17.
 */
public class CustomListViewAdapter extends ArrayAdapter <Route> {

    private int layoutResource;
    private DatabaseHandler dba;
    private Activity activity;
    private ArrayList<Route> routeList = new ArrayList<>();
    public CustomListViewAdapter(Activity act, int resource, ArrayList<Route> data) {
        super(act, resource, data);
        layoutResource = resource;
        activity = act;
        routeList = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return routeList.size();
    }

    @Override
    public Route getItem(int position) {
        return routeList.get(position);
    }

    @Override
    public int getPosition(Route item) {
        return super.getPosition(item);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null || row.getTag() == null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            row = inflater.inflate(layoutResource, null);
            holder = new ViewHolder();
            holder.routeName = (TextView) row.findViewById(R.id.routeName);
            holder.routeOpener = (TextView) row.findViewById(R.id.routeOpener);
            holder.difficulty = (TextView) row.findViewById(R.id.difficulty);
            holder.date = (TextView) row.findViewById(R.id.date);
            holder.thumbnail = (ImageView) row.findViewById(R.id.thumbnail);

            row.setTag(holder);


        }
        else {
            holder = (ViewHolder)row.getTag();
        }

        holder.route = getItem(position);
        holder.routeName.setText(holder.route.getRouteName());
        holder.routeOpener.setText(holder.route.getOpenedBy());
        holder.difficulty.setText(holder.route.getDifficulty());
        holder.date.setText(holder.route.getDate());
        //holder.route.
        //holder.thumbnail.setImageBitmap(Utils.fitImage(60,70, holder.route.getPath()));

        final ViewHolder finalHolder = holder;
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, RouteDetailsActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("userObj", finalHolder.route);
                i.putExtras(mBundle);

                activity.startActivity(i);

            }
        });

        row.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                alert.setTitle("Delete route?");
                alert.setMessage("Are you sure you want to delete route "+ finalHolder.route.getRouteName());
                alert.setNegativeButton("No", null);
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dba = new DatabaseHandler(activity);
                        dba.deleteRoute(finalHolder.route.getRouteId());
                        Toast.makeText(activity, "Deleted route", Toast.LENGTH_SHORT).show();
                        remove(getItem(position));
                        notifyDataSetChanged();

                    }
                });
                alert.show();
                return false;
            }
        });

        return row;
    }

    public class ViewHolder {
        Route route;
        TextView routeName;
        TextView routeOpener;
        TextView difficulty;
        TextView date;
        ImageView thumbnail;
        TextView coord;





    }
}
