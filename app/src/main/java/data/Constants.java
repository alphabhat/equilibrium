package data;

import android.graphics.Bitmap;

/**
 * Created by arubhat on 19/02/17.
 */
public class Constants {

    public static final String DATABASE_NAME = "routeLog";
    public static final String TABLE_NAME = "routeTbl";
    public static final String ROUTE_NAME = "name";
    public static final String OPENER = "opener";
    public static final String PATH = "path";
    public static final String PATH_DRAW = "pathdraw";
    public static final String DIFFICULTY = "diff";
    public static final String DATE = "date";
    public static final int DATABASE_VERSION = 1;
    public static final String KEY_ID = "_id";

}
