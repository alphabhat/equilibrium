package data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.Route;
import util.Utils;

/**
 * Created by arubhat on 19/02/17.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private final ArrayList <Route> routeList = new ArrayList<>();
    public DatabaseHandler(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table
        String CREATE_TABLE = "CREATE TABLE " + Constants.TABLE_NAME + "(" +
                Constants.KEY_ID + " INTEGER PRIMARY KEY, " +
                Constants.ROUTE_NAME + " TEXT, " +
                Constants.OPENER + " TEXT," +
                Constants.DATE + " LONG," +
                Constants.DIFFICULTY + " TEXT, " +
                Constants.PATH + " TEXT, " +
                Constants.PATH_DRAW + " TEXT);";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME);
        onCreate(db);
    }

    //get total items
    public int getTotalItems() {
        int totalItems = 0;
        String query = "SELECT * FROM " + Constants.TABLE_NAME;
        SQLiteDatabase dba = this.getReadableDatabase();
        Cursor cursor = dba.rawQuery(query, null);
        totalItems = cursor.getCount();
        cursor.close();
        return totalItems;
    }

    public void deleteRoute(int id) {
        SQLiteDatabase dba = this.getWritableDatabase();
        dba.delete(Constants.TABLE_NAME, Constants.KEY_ID + " = ?", new String[]{String.valueOf(id)});
        dba.close();
    }

    public void addRoute(Route route) {
        SQLiteDatabase dba = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constants.ROUTE_NAME, route.getRouteName());
        values.put(Constants.OPENER, route.getOpenedBy());
        values.put(Constants.DATE, System.currentTimeMillis());
        values.put(Constants.DIFFICULTY, route.getDifficulty());
        values.put(Constants.PATH, route.getPath());
        values.put(Constants.PATH_DRAW, route.getPathdraw());
        dba.insert(Constants.TABLE_NAME, null, values);
        Log.v("Added route", "Wooo");
        dba.close();

    }

    public ArrayList<Route> getRoutes() {
        routeList.clear();

        SQLiteDatabase dba = getReadableDatabase();
        Cursor cursor = dba.query(Constants.TABLE_NAME, new String[]{
                Constants.KEY_ID,
                Constants.ROUTE_NAME,
                Constants.OPENER,
                Constants.DATE,
                Constants.DIFFICULTY,
                Constants.PATH,
                Constants.PATH_DRAW},
                null, null, null, null,
                Constants.DATE + " DESC ");

        if (cursor.moveToFirst()) {
            do {
                Route route = new Route();
                route.setRouteName(cursor.getString(cursor.getColumnIndex(Constants.ROUTE_NAME)));
                route.setOpenedBy(cursor.getString(cursor.getColumnIndex(Constants.OPENER)));
                route.setDifficulty(cursor.getString(cursor.getColumnIndex(Constants.DIFFICULTY)));
                route.setRouteId(cursor.getInt(cursor.getColumnIndex(Constants.KEY_ID)));
                route.setPath(cursor.getString(cursor.getColumnIndex(Constants.PATH)));
                route.setPathdraw(cursor.getString(cursor.getColumnIndex(Constants.PATH_DRAW)));
                DateFormat dateFormat = DateFormat.getDateInstance();
                String date = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.DATE))).getTime());
                route.setDate(date);
                routeList.add(route);

            }while(cursor.moveToNext());
        }

        cursor.close();
        dba.close();
        return routeList;
    }
}
