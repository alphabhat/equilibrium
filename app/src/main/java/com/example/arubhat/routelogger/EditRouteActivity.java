package com.example.arubhat.routelogger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import data.DatabaseHandler;
import model.Route;
import util.Utils;
import com.loopj.android.http.*;

public class EditRouteActivity extends AppCompatActivity {

    private static final int REQUEST_TAKE_PHOTO = 1 ;
    public static final int REQUEST_GET_DRAW = 2;
    private EditText editRouteName, editOpener,editDiff, editCoord;
    private ImageView editGPS, editSubmit, thumbnail, click;
    private DatabaseHandler dba;
    private Bitmap routeBitmap;
    String mCurrentPhotoPath, mCurrentPhotoWithRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_route);
        dba = new DatabaseHandler(EditRouteActivity.this);

        editRouteName = (EditText)findViewById(R.id.edit_route_name);
        editOpener = (EditText) findViewById(R.id.edit_opener);
        editDiff = (EditText) findViewById(R.id.edit_diff);
        editCoord = (EditText) findViewById(R.id.edit_coord);
        editGPS = (ImageView) findViewById(R.id.edit_gps);
        editSubmit = (ImageView) findViewById(R.id.edit_submit);
        thumbnail = (ImageView) findViewById(R.id.photo);
        click = (ImageView) findViewById(R.id.click);

        thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();

            }
        });
        editSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveRouteToDB();
            }
        });
    }

    private void saveRouteToDB() {
        Route route = new Route();

        route.setRouteName(editRouteName.getText().toString());
            route.setOpenedBy(editOpener.getText().toString());
            route.setDifficulty(editDiff.getText().toString());
            route.setCoordinates(editCoord.getText().toString());
            route.setPath(mCurrentPhotoPath);
            route.setPathdraw(mCurrentPhotoWithRoute);

            if(isNetworkConnected())
            {
                Handler h = new Handler(Looper.getMainLooper());
                h.post(new Runnable() {
                    public void run() {
                        uploadRouteToDatabase();
                    }
                });
                dba.addRoute(route);
                dba.close();
            }
            else
            {
                dba.addRoute(route);
                dba.close();
                startActivity(new Intent(EditRouteActivity.this, DisplayRoutesActivity.class));
            }

        }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

            Intent i = new Intent(EditRouteActivity.this, DrawRoute.class);
            i.putExtra("path", mCurrentPhotoPath);
            startActivityForResult(i, REQUEST_GET_DRAW);
        }

        if (requestCode == REQUEST_GET_DRAW && resultCode == RESULT_OK) {

            mCurrentPhotoWithRoute = data.getStringExtra("drawing_path");
            routeBitmap = Utils.fitImage(300,320,mCurrentPhotoWithRoute);
            thumbnail.setImageBitmap(routeBitmap);
            click.setImageBitmap(null);
        }


        }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void uploadRouteToDatabase() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("routename", editRouteName.getText().toString());
        params.put("openedby", editOpener.getText().toString());
        params.put("difficulty", editDiff.getText().toString());
        params.put("coordinates", editCoord.getText().toString());
        params.put("date", "none");
        try {
            params.put("img", new File(mCurrentPhotoPath));
            params.put("imgwithroute", new File(mCurrentPhotoWithRoute));
        }
        catch(Exception e)
        {
            Log.e("Exception:","Image not found exception");
        }
        client.setResponseTimeout(
                100000
        );
        client.post("https://mighty-earth-16472.herokuapp.com/new/route", params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("Error",responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("Success:",responseString);
                editRouteName.setText("");
                editOpener.setText("");
                editDiff.setText("");
                editCoord.setText("");
                thumbnail.setImageBitmap(null);
                startActivity(new Intent(EditRouteActivity.this, DisplayRoutesActivity.class));
            }
        });
    }
}
