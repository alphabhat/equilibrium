package com.example.arubhat.routelogger;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import data.DatabaseHandler;
import model.Route;
import util.Utils;

public class RouteDetailsActivity extends AppCompatActivity {
    private TextView RouteName, Opener,Diff, Coord, Date;
    private ImageView photo;
    private ImageView GPS;
    private DatabaseHandler dba;
    private Bitmap bitmap, bitmapwithroute;
    private CheckBox draw;
    private Route route;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_details);


        RouteName = (TextView) findViewById(R.id.details_route_name);
        Opener = (TextView) findViewById(R.id.details_opener);
        Diff = (TextView) findViewById(R.id.details_diff);
        Coord = (TextView) findViewById(R.id.details_coord);
        Date = (TextView) findViewById(R.id.details_date);
        GPS = (ImageView) findViewById(R.id.detailsGPS);
        photo = (ImageView) findViewById(R.id.details_photo);
        draw = (CheckBox) findViewById(R.id.details_checkBox);
        draw.setChecked(true);
        route = (Route) getIntent().getSerializableExtra("userObj");

        bitmap = Utils.fitImage(320, 240, route.getPath());
        bitmapwithroute = Utils.fitImage(320, 240, route.getPathdraw());

        photo.setImageBitmap(bitmapwithroute);
        RouteName.setText(route.getRouteName());
        Opener.setText(route.getOpenedBy());
        Diff.setText(route.getDifficulty());

        draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (draw.isChecked()) {
                    photo.setImageBitmap(bitmapwithroute);

                }
                else {
                    photo.setImageBitmap(bitmap);
                }
            }
        });

        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RouteDetailsActivity.this, FullscreenPhoto.class);
                i.putExtra("Path", route.getPath());
                i.putExtra("DrawPath", route.getPathdraw());
                startActivity(i);
            }
        });


    }

}
