package com.example.arubhat.routelogger;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import model.Route;
import util.Utils;

public class FullscreenPhoto extends AppCompatActivity {

    ImageView fullPhoto;
    CheckBox routeOnOff;
    String path, drawPath;
    Bitmap drawOn, drawOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_photo);
        fullPhoto = (ImageView) findViewById(R.id.full_image);
        routeOnOff = (CheckBox) findViewById(R.id.full_route);

        path = getIntent().getStringExtra("Path");
        drawPath = getIntent().getStringExtra("DrawPath");

        drawOn = Utils.fitImage(800,600,drawPath);
        drawOff = Utils.fitImage(800,600,path);

        fullPhoto.setImageBitmap(drawOn);

        routeOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (routeOnOff.isChecked()) {
                    fullPhoto.setImageBitmap(drawOn);
                }
                else {
                    fullPhoto.setImageBitmap(drawOff);
                }
            }
        });




    }
}
