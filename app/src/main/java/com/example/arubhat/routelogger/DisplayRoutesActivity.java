package com.example.arubhat.routelogger;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import data.CustomListViewAdapter;
import data.DatabaseHandler;
import model.Route;
import util.Utils;

public class DisplayRoutesActivity extends AppCompatActivity {

    private DatabaseHandler dba;
    private ArrayList<Route> dbRoute = new ArrayList<>();
    private CustomListViewAdapter routeAdapter;
    private ListView listView;
    private Route myRoute;
    private TextView totalRoutes;
    private int routeCount = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_routes);

        listView = (ListView) findViewById(R.id.display_routes_listview);
        //totalRoutes = (TextView) findViewById(R.id.display_routes_total);

        refreshData();
    }

    void refreshData() {
        dbRoute.clear();

        if(isNetworkConnected())
        {
            routeAdapter = new CustomListViewAdapter(DisplayRoutesActivity.this, R.layout.row, dbRoute );
            listView.setAdapter(routeAdapter);
            Toast.makeText(getApplicationContext(), "Loading routes...",
                    Toast.LENGTH_LONG).show();
            getRouteCount();
        }
        else
        {
            dba = new DatabaseHandler(getApplicationContext());
            ArrayList<Route> routesFromDb = dba.getRoutes();

            int total = dba.getTotalItems();
            //totalRoutes.setText("Total routes:" + String.valueOf(total));

            for (int i=0; i<routesFromDb.size(); i++) {

                String name = routesFromDb.get(i).getRouteName();
                String opener = routesFromDb.get(i).getOpenedBy();
                String diff = routesFromDb.get(i).getDifficulty();
                String coord = routesFromDb.get(i).getCoordinates();
                String date = routesFromDb.get(i).getDate();
                String path = routesFromDb.get(i).getPath();
                String pathdraw = routesFromDb.get(i).getPathdraw();
                int id = routesFromDb.get(i).getRouteId();

                Log.v("Route id:", String.valueOf(id));
                myRoute = new Route(name, id, opener, diff, coord, date, path, pathdraw );

                dbRoute.add(myRoute);

            }
            dba.close();
            routeAdapter = new CustomListViewAdapter(DisplayRoutesActivity.this, R.layout.row, dbRoute );
            listView.setAdapter(routeAdapter);
            routeAdapter.notifyDataSetChanged();
        }
        //setup adapter
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void getRouteCount()
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://mighty-earth-16472.herokuapp.com/count/route", new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Please check your internet connection",
                        Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("route Count is", responseString);
                try {
                   JSONObject object = new JSONObject(responseString);
                   routeCount = object.getInt("count");
                    if(routeCount != -1)
                    {
                        for(int i=1;i<=routeCount;i++)
                        {
                            getRouteDataFromServer(i);
                        }
                    }
                    else
                    {
                        Log.e("error:", "error retrieving routes");
                    }
                }
                catch(Exception e)
                {
                    Log.e("error:","error getting count");
                }
            }
        });
    }

    private void getRouteDataFromServer(int routeNumber)
    {
        final int routeNumber1 = routeNumber;
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://mighty-earth-16472.herokuapp.com/route/"+routeNumber1 , new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("error:", "fetching the route "+routeNumber1+ "from server");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    Log.e("response string", responseString);
                    JSONObject obj = new JSONObject(responseString);
                    final String routeName = obj.getString("routename");
                    final int routeId = routeNumber1;
                    final String openedBy = obj.getString("openedby");
                    final String difficulty = obj.getString("difficulty");
                    final String coordinates = obj.getString("coordinates");
                    final String date = obj.getString("date");
                    final File photo = new File(Environment.getExternalStorageDirectory(), "photo.jpg");

                    Thread tr = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                URL fileUri = new URL("https://mighty-earth-16472.herokuapp.com/routeimg/" + routeNumber1);
                                FileOutputStream fos = new FileOutputStream(photo.getPath());
                                InputStream ins = fileUri.openStream();
                                final byte[] b = new byte[2048];
                                int length;
                                while ((length = ins.read(b)) != -1) {
                                    fos.write(b, 0, length);
                                }
                                ins.close();
                            }
                            catch (Exception e)
                            {
                                Toast.makeText(getApplicationContext(), "Please check your internet connection",
                                        Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }
                    });
                    tr.start();
                    tr.join();
                    final String path = photo.getPath();
                    Route route = new Route(routeName, routeId, openedBy, difficulty, coordinates, date, path, path);
                    dbRoute.add(route);
                    routeAdapter.notifyDataSetChanged();

                }
                catch(Exception e)
                {
                    Toast.makeText(getApplicationContext(), "Please check your internet connection",
                            Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
    }
}
